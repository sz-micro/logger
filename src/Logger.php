<?php

namespace GranitSDK;

class Logger
{
	const SEVERITY_INFO = 'INFO';
	const SEVERITY_WARNING = 'WARN';
	const SEVERITY_ERROR = 'ERROR';

	protected static $instance;

	private $name = 'default';

	public function __construct()
	{
	}

	public static function get()
	{
		if (!self::$instance) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function info($message, $context = [])
	{
		$this->log(self::SEVERITY_INFO, $message, $context);
	}

	public function error($message, $context = [])
	{
		$this->log(self::SEVERITY_ERROR, $message, $context);
	}

	public function warning($message, $context = [])
	{
		$this->log(self::SEVERITY_WARNING, $message, $context);
	}

	public function log($type, $message, $context = [])
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 9);
		// Skippeljuk az elso 3-at mert az mar a logolasnak a folyamata
		array_shift($backtrace);
		array_shift($backtrace);
		array_shift($backtrace);

		$json = [
			'message' => $message,
			'context' => $context,
			'severity' => $type,
			'caller' => !empty($_SERVER['HTTP_CALLER_APP']) ? $_SERVER['HTTP_CALLER_APP'] : '',
			'httpRequest' => [
				'requestMethod' => $_SERVER['REQUEST_METHOD'] ?? '',
				'requestBody' => file_get_contents('php://input'),
				'requestUrl' => $_SERVER['REQUEST_URI'] ?? '',
				'requestQuery' => $_SERVER['QUERY_STRING'] ?? '',
				'userAgent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
				'remoteIp' => $_SERVER['REMOTE_ADDR'] ?? '',
				'post' => $_POST ?? ''
			],
			'backtrace' => $backtrace,
		];

		file_put_contents('php://stdout', json_encode($json) . "\n");
	}
}